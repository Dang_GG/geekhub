String firstOption = "first"
String secondOption = "second"

String actual = true ? firstOption : secondOption
String expected = firstOption

assert actual == expected

actual = false ? firstOption : secondOption
expected = secondOption

assert actual == expected

actual = firstOption ?: secondOption
expected = firstOption 

assert actual == expected

actual = null ?: secondOption
expected = secondOption 

